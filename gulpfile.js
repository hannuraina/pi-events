var gulp = require('gulp');
var concat = require('gulp-concat');
var chalk = require('chalk');
var sass = require('gulp-sass');
var util = require('gulp-util');
var plumber = require('gulp-plumber');
var browserify = require('browserify');
var bulkify = require('bulkify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var replace = require('gulp-replace');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var watchify = require('watchify');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync').create()
var karmaServer = require('karma').Server;
var eslint = require('gulp-eslint');
var proxyMiddleware = require('http-proxy-middleware');
var fallback = require('connect-history-api-fallback')

// app path
var appPath = 'app';
var destPath = 'build';
var nodeModules = 'node_modules';
var vendorPath = 'vendor';
var watch = false;

var src = {
	flags: './' + nodeModules + '/flag-icon-css/flags/**/*',
	webpages: './' + appPath + '/*.html',
	sass: './' + appPath + '/scss/**/*',
	img: './' + appPath + '/img/**/*',
	fonts: './' + appPath + '/fonts/*',
	vendor: './' + vendorPath + '/*.js'
}

gulp.task('default', ['build']);

gulp.task('build', ['html', 'sass', 'img', 'vendor', 'fonts', 'scripts', 'lint']);

gulp.task('deploy', ['prod', 'build', 'replace']);

gulp.task('prod', function () {
	process.env.NODE_ENV = 'production';
});

gulp.task('pre-watch', function () {
	watch = true;
});

gulp.task('watch', ['pre-watch', 'build'], function () {
	gulp.watch(src.webpages, ['html']).on('change', function () {
		browserSync.reload();
	});
	gulp.watch(src.vendor, ['vendor']).on('change', function() {
		browserSync.reload();
	});
	gulp.watch(src.sass, ['sass']);
	gulp.watch(src.img, ['img']);
});

gulp.task('html', function () {
	return gulp.src(src.webpages)
		.pipe(gulp.dest(destPath));
});

gulp.task('replace', function() {
	gulp.src(destPath + '/index.html')
		.pipe(replace('licenseKey:"",applicationID:""', 'licenseKey:"62aa59f626",applicationID:"21095258"'))
		.pipe(gulp.dest(destPath));
});

gulp.task('sass', function () {
	return gulp.src(src.sass)
		.pipe(sourcemaps.init())
		.pipe(plumber(function (error) {
			util.beep();
			console.log(
				chalk.gray('\n====================================\n') +
				'[' + chalk.magenta('SASS') + '] ' + chalk.red.bold('Error') +
				chalk.gray('\n------------------------------------\n') +
				chalk.yellow('Message:\n  ') +
				error.messageFormatted +
				chalk.gray('\n------------------------------------\n') +
				chalk.yellow('Details:') +
				chalk.green('\n  Line: ') + error.line +
				chalk.green('\n  Column: ') + error.column +
				chalk.gray('\n====================================\n')
			);
			this.emit('end');
		}))
		.pipe(sass({
			outputStyle: 'compressed',
			includePaths: [
				nodeModules + '/node-normalize-scss',
				nodeModules + '/flag-icon-css/sass',
				nodeModules + '/bootstrap-sass/assets/stylesheets'
			]})
		)
		.pipe(sourcemaps.write())
		.pipe(concat('app.css'))
		.pipe(gulp.dest(destPath + '/css'))
		.pipe(browserSync.stream());
});

gulp.task('img', function () {
	return gulp.src([
			src.img,
			src.flags
		])
		.pipe(gulp.dest(destPath + '/img'));
});

gulp.task('fonts', function () {
	return gulp.src(src.fonts)
		.pipe(gulp.dest(destPath + '/fonts'));
});

gulp.task('vendor', function() { 
	return gulp.src([
			nodeModules + '/jquery/dist/jquery.min.js',
			nodeModules + '/es6-promise/dist/es6-promise.min.js',
			vendorPath  + '/**/*.js'
		])
		.pipe(concat('vendor.js'))
		.pipe(gulp.dest(destPath + '/js'));
});

gulp.task('scripts', function () {
	var bro = browserify({
		entries: './' + appPath + '/js/app.js',
		debug: true,
		alias: ['react:react-addons', 'React:-addons'],
		transform: [babelify],
		extensions: ['.js', '.jsx']
	})
	.transform(bulkify, { global: true })
	.require('respond');

	// our javascript bundler
	var bundler = (watch) ? watchify(bro) : bro;

	// when the bundler updates
	bundler.on('update', function () {
		// call our rebundler again
		rebundle(bundler);
	});

	// our rebundle function
	function rebundle(bundler) {
		util.log('Browserify is bundling...');
		// tell browserify we are compiling
		browserSync.notify('Browserify is bundling...');
		// default bundler to not have an error
		bundler.error = false;
		// send our bundler bundle back
		return bundler.bundle()
			.on('error', function (error) {
				// set bundler error to true to check for later
				bundler.error = true;
				// beep and give us the error
				util.beep();
				// tell browserify we got an error
				browserSync.notify('Browserify Error!');
				// log the message
				console.log(
					chalk.gray('\n====================================\n') +
					'[' + chalk.blue('Browserify') + '] ' + chalk.red.bold('Error') +
					chalk.gray('\n------------------------------------\n') +
					chalk.yellow('Message:\n  ') + error.message +
					chalk.gray('\n====================================\n')
				);
			})
			.pipe(source('app.js'))
			.pipe(buffer())
			.pipe(sourcemaps.init({ loadMaps: true }))
			.pipe(sourcemaps.write('./'))
			.pipe(gulp.dest(destPath + '/js'))
			.on('end', function () {
				// don't do anything if we have an error
				if (!bundler.error) {
					// we are done bundling
					util.log('Browserify finished bundling!');
					// tell browserify we got an error
					browserSync.notify('Browserify finished bundling!');
					// uglify the file
					gulp.src(destPath + '/js/app.js')
						.pipe(uglify())
						.pipe(rename({ extname: '.min.js' }))
						.pipe(gulp.dest(destPath + '/js'));

					// tell browser sync to reload the page
					browserSync.reload();
				}
			});
	}

	// call the rebundle to bundle the app
	return rebundle(bundler);
});

gulp.task('lint', function () {
  return gulp.src(['app/js/**/*.js'])
    .pipe(eslint({ useEslintrc: true }))
    .pipe(eslint.format())
		.pipe(eslint.failOnError());
});

gulp.task('serve', ['watch'], function () {
	var proxy = proxyMiddleware(['/api', '/uploads'], {
		target: 'http://local.conferences.pionline.com',
		changeOrigin: true
	});

	browserSync.init({
		server: {
			baseDir: destPath,
			middleware: [ proxy, fallback() ]
		}
	})
});

gulp.task('test', function (done) {
  new karmaServer({configFile: __dirname + '/karma.conf.js'}, done).start();
});
