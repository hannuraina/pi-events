'use strict';

import React from 'react/addons';
import StubRouterContext from '../utils/stubRouterContext';
import EventsIndex from '../../app/js/components/events/index';

let TestUtils = React.addons.TestUtils;

describe('EventsIndex', () => {
  let component;
  let props = {};
  let ComponentWithRouting = StubRouterContext(EventsIndex, props);

  beforeEach(() => {
    component = TestUtils.renderIntoDocument(<ComponentWithRouting />);
  });

  it('render', () => {
    expect(component).toEqual(jasmine.anything());
  });
});
