'use strict';

module.exports = function(config) {
  config.set({

    // The entry point for our test suite
    basePath: '.',

    // Add any browsers here
    browsers: ['PhantomJS'],
    frameworks: ['browserify', 'jasmine' ],
    plugins: [
      'karma-phantomjs-launcher',
      'karma-jasmine',
      'karma-browserify'
    ],

    files: [
      'test/**/*.spec.js'
    ],

    preprocessors: {
      'test/**/*.spec.js': ['browserify']
    },

    browserify: {
      debug: true,
      transform: [ 'babelify' ],
      extensions: ['.js', '.jsx']
    },

    reporters: ['progress'],
    port: 9876,
    colors: true,
    singleRun: true // exit after tests have completed

  });
};
