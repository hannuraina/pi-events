set :application, "events"

set :scm, :git
set :user, "www-data"
set :repository, "git@bitbucket.org:craininteractive/pi-#{application}.git"
set :copy_exclude, [".git", ".DS_Store", ".gitignore", ".gitmodules", "Capfile", "deploy.rb"]
set :instart_auth, "jmcclure@pionline.com:Ni63eR905k0"
set :instart_url, "https://api.instartlogic.com/crain/v1/cache/purge"
set :use_sudo, false
set :keep_releases, 1

task :www do
  server "apps2.pionline.com", :app
  set :stage, "www"
  set :deploy_to, "/var/#{stage}/#{application}"
  set :instart_target, "pionline.com/events"
  set :deploy_via, :remote_cache
  set :branch, "master"
  before "deploy:update_code", "git:guard_upstream"
end

task :qa do
  server "qa-apps.pionline.com", :app
  set :stage, "qa"
  set :deploy_to, "/var/#{stage}/#{application}"
  set :instart_target, "qa.pionline.com/events"
  set :deploy_via, :remote_cache
  set :branch do
    branch = `git rev-parse --abbrev-ref HEAD`.chomp
    branch
  end
  before "deploy:update_code", "git:push_branch"
  after "deploy:update_code", "git:delete_remote_branch"
end

task :dev do
  server "dev-apps.pionline.com", :app
  set :stage, "dev"
  set :deploy_to, "/var/#{stage}/#{application}"
  set :instart_target, "dev.pionline.com/events"
  set :deploy_via, :remote_cache
  set :branch do
    branch = `git rev-parse --abbrev-ref HEAD`.chomp
    branch
  end
  before "deploy:update_code", "git:push_branch"
  after "deploy:update_code", "git:delete_remote_branch"
end

namespace :git do
  task :push_branch do
    set :orig_branch, fetch(:branch)
    set :branch, fetch(:release_name)
    `git checkout -b #{release_name}`
    `git push origin #{release_name}`
  end
  task :delete_remote_branch do
    `git push origin :#{release_name}` #remove remote tmp branch
    `git checkout -f #{orig_branch}` #switch back to original branch
    `git branch -D #{release_name}` #delete tmp push branch
  end
end

namespace :shared do
  task :link_admin do
    run "ln -s /var/#{stage}/admin/shared/uploads #{release_path}/build/uploads"
  end
end

namespace :instart do
  task :flush do
    # clear all prefixes
    puts "Flushing instart cache PREFIX:#{instart_target}"
    puts `curl -s -u #{instart_auth} \
      #{instart_url} \
      -X POST \
      -H "Content-Type: application/json" \
      -d "{"""purge_request""":{"""action""":"""PURGE""","""uris""":[{"""match_mode""":"""PREFIX_LITERAL""","""uri_pattern""":"""#{instart_target}"""}]}}"`

    # clear landing page
    puts "Flushing instart cache EXACT:#{instart_target}"
    puts `curl -s -u #{instart_auth} \
      #{instart_url} \
      -X POST \
      -H "Content-Type: application/json" \
      -d "{"""purge_request""":{"""action""":"""PURGE""","""uris""":[{"""match_mode""":"""EXACT_LITERAL""","""uri_pattern""":"""#{instart_target}"""}]}}"`
    end
end

namespace :gulp do
  task :build do
    run "cd #{release_path} && npm install && gulp deploy"
  end
end

before "deploy:update_code", "deploy:setup", "git:guard_committed"
after "deploy:create_symlink", "gulp:build"
after "deploy:update", "shared:link_admin", "deploy:cleanup"
