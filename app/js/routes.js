'use strict';

import React from 'react';
import {Route, IndexRoute, Redirect} from 'react-router';

import App        from './app';
import Landing    from './components/conference/landing';
import Index      from './components/conference/index';
import Speakers   from './components/conference/speakers';
import Agendas    from './components/conference/agendas';
import Advisors   from './components/conference/advisors';
import Resources  from './components/conference/resources';
import Sponsors   from './components/conference/sponsors';
import Exhibitors from './components/conference/exhibitors';
import Venues     from './components/conference/venues';
import Contact    from './components/conference/contact';
import Multimedia from './components/conference/multimedia';
import Register   from './components/conference/register';

const routes = <Route path="/" component={App}>
    <Route path="conference/:conference_id/:year" component={Index} >
      <IndexRoute component={Landing} />
      <Route path="agenda" component={Agendas}>
        <Route path=":agenda_id" />
      </Route>
      <Route path="resources" component={Resources} />
      <Route path="venue" component={Venues}>
        <Route path=":venue_id" />
      </Route>
      <Route path="contact" component={Contact}/>
      <Route path="register" component={Register} />
      <Route path="multimedia" component={Multimedia} />
      <Route path="sponsors" component={Sponsors}>
        <Route path=":sponsor_id" />
      </Route>
      <Route path="exhibitors" component={Exhibitors}>
        <Route path=":sponsor_id" />
      </Route>
      <Route path="speakers" component={Speakers} >
        <Route path=":speaker_id" />
      </Route>
      <Route path="advisors" component={Advisors} />
    </Route>
  </Route>

export default routes;
