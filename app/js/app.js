'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import {Router, History} from 'react-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';

// global polyfill for older browsers and IE
require('es6-promise').polyfill(); 

import Routes from './routes';

class App extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return <div>{this.props.children}</div>
  }
};

// initialize router
let history = createBrowserHistory();
ReactDOM.render(
  <Router history={history} routes={Routes} onUpdate={() => window.scrollTo(0, 0)} />,
  document.getElementById('app')
);

export default App;
