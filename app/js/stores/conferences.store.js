'use strict';

import _ from 'lodash';
import alt from '../alt';
import {createStore, datasource, bind} from 'alt/utils/decorators';
import ConferenceActions from '../actions/conference.actions';
import ConferencesSource from '../sources/conferences.source';

@createStore(alt)
@datasource(ConferencesSource)
class ConferencesStore {

  constructor() {
    this.upcoming   = [];
    this.recent     = [];
  }

  // fetch all conferences
  @bind(ConferenceActions.loadAll)
  onLoadAll(props) {
    if (!this.getInstance().isLoading()) {
      this.getInstance().fetch(props);
    }
  }

  @bind(ConferenceActions.loadedAll)
  onLoadedAll(results) {
    let date = parseInt(new Date().getTime()/1000); // used to separate upcoming and recent events
    _.each(results, result => {
      // attach title to description to simplify search
      result.description = [
        ['<span class=\'hidden\'>', result.name, '</span>'].join(''),
        result.conference_description
      ].join('');

      result.cities = this.resolveCities(result.locations);
    });

    this.upcoming = _.filter(results, r => { return r.date > date; });
    this.recent   = _.filter(results, r => { return r.date < date; });
  }

  resolveCities = (locations) => {
    let cities = [];
    _.map(locations, location => { cities.push(location); });
    return cities;
  }
}

export default ConferencesStore;
