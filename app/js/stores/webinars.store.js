'use strict';

import _ from 'lodash';
import alt from '../alt';
import {createStore, datasource, bind} from 'alt/utils/decorators';
import WebinarActions from '../actions/webinar.actions';
import WebinarsSource from '../sources/webinars.source';

@createStore(alt)
@datasource(WebinarsSource)
class WebinarsStore {

  constructor() {
    this.upcoming    = [];
    this.recent      = [];
  }

  @bind(WebinarActions.loadAll)
  onLoadAll(props) {
    if (!this.getInstance().isLoading()) {
      this.getInstance().fetch(props);
    }
  }

  @bind(WebinarActions.loadedAll)
  onloadedAll(results) {
    let date = parseInt(new Date().getTime()/1000);
    // map year data from date
    _.each(results.data, result => { result.year = new Date(result.date * 1000).getFullYear(); });
    this.upcoming = _.filter(results.data, r => { return r.date > date; });
    this.recent = _.filter(results.data, r => { return r.date < date; });
  }
}

export default WebinarsStore;
