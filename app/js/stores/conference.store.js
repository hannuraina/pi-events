'use strict';

import _ from 'lodash';
import alt from '../alt';
import {createStore, datasource, bind} from 'alt/utils/decorators';
import ConferenceActions from '../actions/conference.actions';
import ConferenceSource from '../sources/conference.source';

@createStore(alt)
@datasource(ConferenceSource)
class ConferenceStore {

  constructor() {
    this.current   = {};
  }

  // fetch individual conference
  @bind(ConferenceActions.load)
  onLoad(props) {
    if (!this.getInstance().isLoading()) {
      this.getInstance().fetch(props);
    }
  }

  @bind(ConferenceActions.loaded)
  onLoaded(result) {
    this.current                   = result ? result[0] : [];
    this.current.cities            = _(this.current.locations).sortBy('start').values().value();
    this.current.hasAdvisoryBoard  = !!_(this.current.speakers).find({ advisory_board: true });
    this.current.hasExhibitors     = !!_(this.current.sponsors).find({ level: 'exhibitor' });
    this.current.hasResources      = !!(this.current.white_papers || this.current.supplements);
    this.current.hasSpeakers       = !!this.current.speakers;
    this.current.hasSponsors       = !!this.current.sponsors;
    this.current.hasMultimedia     = this.current.media && this.current.media.slideshows;
  }
}

export default ConferenceStore;
