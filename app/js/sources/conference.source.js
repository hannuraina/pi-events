'use strict';

import jsonp from 'jsonp-promise';
import query from 'query-string';
import ConferenceActions from '../actions/conference.actions';

let base   = '/api/v1/events';
let key    = '7803c9c9f39e6d58a831a8f10df0d1394a94e38d';
let limit  = 1;

const ConferenceSource = {
  fetch: {
    remote(state, props) {
      return jsonp([base, query.stringify({
        key:           key,
        limit:         props.limit,
        event_type:    props.type,
        conference_id: props.conference_id,
        year:          props.year,
        fields:        props.fields,
        data:          true,
        order:         'date:DESC'
      })].join('?')).promise;
    },
    shouldFetch(state) {
      return true;
    },
    loading: ConferenceActions.loading,
    success: ConferenceActions.loaded
  }
};

export default ConferenceSource;
