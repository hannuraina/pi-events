'use strict';

import jsonp from 'jsonp-promise';
import query from 'query-string';
import ConferenceActions from '../actions/conference.actions';

let base   = '/api/v1/events';
let key    = '7803c9c9f39e6d58a831a8f10df0d1394a94e38d';
let limit  = 500;

const ConferencesSource = {
  fetch: {
    remote(state, props) {
      return jsonp([base, query.stringify({
        key:        key,
        limit:      limit,
        event_type: props.type,
        page:       props.page,
        fields:     props.fields,
        order:      'date:DESC'
      })].join('?')).promise;
    },
    shouldFetch(state) {
      return true;
    },
    success: ConferenceActions.loadedAll
  }
};

export default ConferencesSource;
