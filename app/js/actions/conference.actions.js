'use strict';

import alt from '../alt';
import {createActions} from 'alt/utils/decorators';

@createActions(alt)
class ConferenceActions {
  constructor() {
    this.generateActions(
      'load',
      'loaded',
      'loadAll',
      'loadedAll'
    );
  }
}

export default ConferenceActions;
