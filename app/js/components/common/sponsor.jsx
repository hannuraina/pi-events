'use strict';

import _ from 'lodash';
import React from 'react';
import {Link} from 'react-router';

class Sponsor extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let sponsorUrl = [
      this.props.path,
      this.props.sponsor.level === 'exhibitor' ? 'exhibitors' : 'sponsors',
      this.props.sponsor._id
    ].join('/');

    let sponsorLogo = '/' + this.props.sponsor.logo;

    return <div className="sponsor clearfix">
        { !this.props.showBio ? null : <div className="name company-name">{this.props.sponsor.name}</div> }
        <div className="detail clearfix">
          { !this.props.sponsor ? null : <Link className="logo" to={sponsorUrl}><img src={sponsorLogo}></img></Link> }
          <div className="bio">
            { !this.props.showBio ? null :
              <div>
                <div className="company">Contact</div>
                <div className="contact">
                  <div className="name">{this.props.sponsor.contact_name}</div>
                  <div>{this.props.sponsor.contact}</div>
                  <Link to={'mailto:' + this.props.sponsor.contact_email}>{this.props.sponsor.contact_email}</Link>
                  <div>{this.props.sponsor.contact_phone}</div>
                  {this.props.sponsor.url}
                </div>
                <div>{this.props.sponsor.text}</div>
              </div>
            }
          </div>
        </div>
      </div>
  }
}

export default Sponsor;
