'use strict';

import _ from 'lodash';
import React from 'react';

class Listing extends React.Component {
  propTypes: {
    limit:    React.PropTypes.number.required,
    elements: React.PropTypes.array.required
  }

  constructor(props) {
    super(props);
    this.state = {
      count: this.props.limit,
      hasMore: true
    };
  }

  loadMore = (evt) => {
    this.setState({
      count:   this.state.count + this.props.limit,
      hasMore: this.state.count + this.props.limit <= this.props.elements.length
    });
  }

  render() {
    let text = this.props.buttonText ? this.props.buttonText : 'view more';

    return <div className="listing clearfix">
      <div className="title sub">{this.props.title}</div>
      <div>
        { this.props.limit ? this.props.elements.slice(0, this.state.count) : this.props.elements }
      </div>
      { !this.props.limit ? null :
        this.props.buttonLink ?
          <a className="button button-green" href={this.props.buttonLink}>{text}</a> :
            this.state.hasMore ? <a className="button button-green" onClick={this.loadMore}>{text}</a> : null
      }
    </div>
  }
}

Listing.defaultProps = {
  limit:    0,
  elements: []
};

export default Listing;
