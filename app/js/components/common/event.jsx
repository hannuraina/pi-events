'use strict';

import _ from 'lodash';
import React from 'react';
import {Link} from 'react-router';
import moment from 'moment';

class Event extends React.Component {

  constructor(props) {
    super(props);
  }

  renderDate = (city, format, isMultiCity) => {
    let fmt = format ? format : 'MMMM DD';
    if (!city) return moment.unix(city.date).utcOffset('+0500').format(format);
    
    return !format && city.end && city.start !== city.end ?
      [
        [moment.unix(city.start).utcOffset('+0500').format(fmt), moment.unix(city.end).utcOffset('+0500').format('DD')].join('-'),
        isMultiCity ? 'multicity' : city.city
      ].join(' • ') :
      [moment.unix(city.start).utcOffset('+0500').format(fmt), isMultiCity ? 'multicity' : city.city].join(' • ')
  }

  render() {
    let eventLink = this.props.event.name ?
      '/conference/' + this.props.event.conference_id + '/' + this.props.event.year :
      this.props.event.link;

    let marketingImg = this.props.event.marketing_img ?
      '/' + this.props.event.marketing_img :
      null;

    return <div className={ this.props.inverse ? 'event clearfix inverse' : 'event clearfix'}>
      { this.props.image && eventLink && marketingImg ?
        <a href={eventLink}><img className="event-logo" src={marketingImg}/></a> :
        null
      }
      <div className="content">
        { !this.props.locations ?
          <div className="info">
            { _.size(this.props.event.cities) > 1 ?
              <time className="date" key={this.props.event.cities[0]._id}>
                {this.renderDate(this.props.event.cities[0], 'MMMM YYYY', true)}
              </time> :
              _.map(this.props.event.cities, (city) => {
                return <time className="date" key={city._id}>{this.renderDate(city)}</time>
            })}
            <div className="title">
              { eventLink ?
                <a href={eventLink}>
                  {this.props.event.name ? this.props.event.name  : this.props.event.title}
                </a> :
                null
              }
            </div>
            { !this.props.description ? null :
              <div className="description" dangerouslySetInnerHTML={{__html: this.props.event.description}}></div>
            }
          </div> :
          <div className="locations">
            { _.map(this.props.event.cities, (city) => {
              return <Link to={eventLink + '/agenda/' + city.city} className="location" key={city._id}>
                {this.renderDate(city, this.props.formatDate)}
                <span className="venue"><br/>{ city.venue }</span>
                <span className="icon-arrow-right"></span>
              </Link>
            })}
          </div>
        }
      </div>
    </div>
  }
}

export default Event;
