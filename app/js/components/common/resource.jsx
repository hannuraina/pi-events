'use strict';

import _ from 'lodash';
import React from 'react';
import {Link} from 'react-router';

class Resource extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let sponsor =  _(this.props.conference.current.sponsors).find({ _id: this.props.resource.sponsor_name });
    let image   = '/' + this.props.resource.image;
    let link    = '/' + this.props.resource.paper;

    return <div className="resource clearfix">
      {!this.props.showPhoto ? null : <Link to={link}><img src={image}></img></Link>}
      <div className="info">
        <div className="name"><Link to={link}>{this.props.resource.title}</Link></div>
        { this.props.type !== 'whitepaper' ? null :
          <div className="sponsor-info">
            { !this.props.resource.authors ? null :
              <div><span className="company">Authors</span><span>{this.props.resource.authors}</span></div>
            }
            { !sponsor ? null :
              <div><span className="company">Firm</span><span>{sponsor.name}</span></div>
            }
          </div>
        }
        <div className="description" dangerouslySetInnerHTML={{__html: this.props.resource.summary}}/>
        <a className="button button-green" href={link}>{'Download ' + this.props.type}
          <span className="icon-arrow-right"></span>
        </a>
      </div>
    </div>
  }
}

export default Resource;
