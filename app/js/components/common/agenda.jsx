'use strict';

import _ from 'lodash';
import React from 'react';

import Listing from './listing';
import Speaker from './speaker';

class Agenda extends React.Component {

  constructor(props) {
    super(props);
  }

  renderSpeakers = (speakers) => {
    return _(speakers)
    .filter((v) => { return v; })
    .sortBy((v) => { return _.last(v.split('-')); })
    .map((v) => {
      let speaker  = this.props.conference.current.speakers[v];
      return !speaker ? null : <Speaker key={v} speaker={speaker} {...this.props} />
    }).value();
  }

  render() {
    let type         = _.first(this.props.agenda.event_name.split(':')).trim();
    let name         = _.slice(this.props.agenda.event_name.split(':'), 1).join(':').trim();
    let mods         = this.renderSpeakers(this.props.agenda.event_moderators);
    let speakers     = this.renderSpeakers(this.props.agenda.event_speakers);
    let panelists    = this.renderSpeakers(this.props.agenda.event_panelists);

    return <div className="agenda wrap-one clearfix">
      <div className="agenda-location">
        <time className="date">{this.props.agenda.start_time}-{this.props.agenda.end_time}</time>
        <div className="space">{this.props.agenda.event_space}</div>
        { !this.props.agenda.presentation ? null :
          <div className="presentation">
            <a href={'/' + this.props.agenda.presentation}>
              <span className="icon-download"></span>Presentation
            </a>
          </div>
        }
      </div>
      <div className="agenda-description">
        <div className="title">{type}</div>
        { type !== name ? <div className="name">{name}</div> : null }
        <div className="description" dangerouslySetInnerHTML={{__html: this.props.agenda.description_long }}></div>
        { mods.length ? <Listing elements={mods} title={ 'Moderator' + (mods.length === 1 ? '' : 's')} /> : null }
        { speakers.length ? <Listing elements={speakers} title={ 'Speaker' + (speakers.length === 1 ? '' : 's')} /> : null }
        { panelists.length ? <Listing elements={panelists} title={ 'Panelist' + (panelists.length === 1 ? '' : 's')} /> : null }
      </div>
    </div>
  }
}

export default Agenda;
