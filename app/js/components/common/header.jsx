'use strict';

import _ from 'lodash';
import React from 'react';
import moment from 'moment';
import {IndexLink, Link} from 'react-router';
import {Nav, MenuItem, Navbar, NavDropdown} from 'react-bootstrap';

class Header extends React.Component {

  constructor(props) {
    super(props);
  }

  renderRegister = (reg) => {
    let url        = null;
    let buttonText = reg && reg.buttonText ? reg.buttonText : 'Register';

    if (!reg)           return url;
    else if (reg.url)   url = reg.url;
    else if (reg.email) url = 'mailto:' + reg.email;

    return reg.html ?
      <Link className="register" to={this.props.path + '/register'}>{buttonText}</Link> :
      <a className="register" href={url}>{buttonText}</a>;
  }

  renderMobileMenu = () => {
    setTimeout(() => {
      if (typeof $ === 'undefined') return;
      let win        = $(window);
      let hamburger  = $('.main-hamburger');
      let nav        = $('#main-nav ul');
      let header     = $('.header-wrap');
      let main       = $('main');
      let mainHeader = $('.header-wrap #main-header');

      if (!$._data(hamburger[0]).events) {
        hamburger.click(() => { hamburger.toggleClass('close'), nav.toggleClass('active') });
        nav.click(() => { hamburger.toggleClass('close'), nav.toggleClass('active') });

        win.scroll(() => {
        	let scrollPosition = win.scrollTop();
        	scrollPosition >= 57 ?
            (header.addClass('fixed'), mainHeader.addClass('mobile'), main.addClass('fixed-margin')) :
            (header.removeClass('fixed'), mainHeader.removeClass('mobile'), main.removeClass('fixed-margin'));
        });
      }
    });
  }

  render() {
    let mobile = this.renderMobileMenu();
    let menu   = '<!--[if !IE]><!--><a href="//pionline.com"><?xml version="1.0" encoding="utf-8"?>\
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"\
                       viewBox="0 0 128.1 18" enable-background="new 0 0 128.1 18" xml:space="preserve">\
                      <g>\
                        <path class="color" d="M1.2,1.7c0-0.3-0.1-0.5-0.2-0.7L0.2,0.4C0.1,0.3,0,0.2,0,0.1C0,0.1,0.1,0,0.2,0l4.1,0c2.1,0,3.5,1,3.5,4.7\
                          c0,3.1-1.1,5-3.5,5H3.3l0,6.6c0,0.3,0,0.5,0.2,0.7l1,0.7c0.1,0.1,0.2,0.1,0.2,0.2c0,0.1-0.1,0.1-0.2,0.1H0.2C0.1,18,0,17.9,0,17.9\
                          c0-0.1,0.1-0.1,0.2-0.2L1,16.9c0.2-0.2,0.2-0.4,0.2-0.7L1.2,1.7z M3.3,8.4h0.8c1.1,0,1.5-1.1,1.5-3.8c0-2.7-0.3-3.5-1.5-3.5H3.3\
                          L3.3,8.4z"/>\
                        <path class="color" d="M9.7,12.8c0,3,0.5,4.2,1.7,4.2c0.9,0,1.3-0.4,1.9-1.1c0.1-0.1,0.1-0.1,0.2-0.1c0.1,0,0.1,0,0.1,0.1\
                          C13.2,17,12.3,18,11,18c-2.4,0-3.5-2.2-3.5-7.5c0-3.6,1.2-5.8,3.1-5.8c1.6,0,2.9,1,2.9,4.5l0,1.5c0,0.2-0.1,0.3-0.2,0.3H9.7\
                          L9.7,12.8z M11.6,10l0-2.4c0-1.3-0.4-1.9-1-1.9c-0.7,0-1,0.7-1,1.9l0,2.4H11.6z"/>\
                        <path class="color" d="M16.2,5.5L16.2,5.5c0.5-0.5,1-0.8,1.9-0.8c1.3,0,2.2,0.8,2.2,2.4l0,9.6c0,0.1,0,0.2,0.1,0.2l0.8,0.8\
                          c0.1,0,0.1,0.1,0.1,0.2c0,0.1-0.1,0.1-0.2,0.1h-3.5c-0.1,0-0.2,0-0.2-0.1c0,0,0.1-0.1,0.1-0.2l0.8-0.8c0.1,0,0.1-0.1,0.1-0.2l0-9.6\
                          c0-0.9-0.3-1.4-0.9-1.4c-0.6,0-1.3,0.8-1.3,2.1l0,8.9c0,0.1,0,0.2,0,0.2l0.8,0.8c0.1,0,0.1,0.1,0.1,0.2c0,0.1-0.1,0.1-0.2,0.1h-3.5\
                          c-0.1,0-0.2,0-0.2-0.1c0,0,0-0.1,0.1-0.2l0.8-0.8c0.1,0,0.1-0.1,0.1-0.2l0-10.9c0-0.1,0-0.1-0.1-0.2l-0.8-0.7\
                          c-0.1-0.1-0.1-0.1-0.1-0.2c0-0.1,0.1-0.1,0.2-0.1h2.6c0.1,0,0.1,0,0.1,0.1L16.2,5.5z"/>\
                        <path class="color" d="M26.2,9.4c0,0.1-0.1,0.2-0.2,0.2c-0.1,0-0.1-0.1-0.2-0.2c-0.2-0.7-0.3-1.5-0.8-2.5\
                          c-0.3-0.8-0.7-1.2-1.2-1.2c-0.6,0-0.7,0.6-0.7,1.3c0,1,0.6,2.4,1.6,3.9c1,1.5,1.7,3.1,1.7,4.7c0,1.5-1.2,2.4-2.3,2.4\
                          c-0.8,0-1.2-0.3-1.6-0.8l-0.7,0.6c-0.1,0-0.2,0.1-0.3,0.1c-0.1,0-0.1-0.1-0.1-0.2l0-4.6c0-0.1,0-0.2,0.1-0.2c0.1,0,0.1,0.1,0.2,0.2\
                          c0.4,1.9,1,3.7,2,3.7c0.6,0,0.9-0.4,0.9-1c0-1.5-0.9-3.1-1.7-4.3c-1-1.4-1.5-2.9-1.5-4.4c0-1.6,1.1-2.6,2-2.6\
                          c0.6,0,1.1,0.3,1.6,0.8l0.8-0.6c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1,0.1,0.1,0.2L26.2,9.4z"/>\
                        <path class="color" d="M27.8,5.8c0-0.1,0-0.1-0.1-0.2l-0.8-0.7c-0.1-0.1-0.1-0.1-0.1-0.2c0-0.1,0.1-0.1,0.2-0.1h2.6\
                          c0.1,0,0.1,0,0.1,0.1l0,11.9c0,0.1,0,0.2,0.1,0.2l0.8,0.8c0.1,0,0.1,0.1,0.1,0.2c0,0.1-0.1,0.1-0.2,0.1H27c-0.1,0-0.2,0-0.2-0.1\
                          c0,0,0.1-0.1,0.1-0.2l0.8-0.8c0.1,0,0.1-0.1,0.1-0.2L27.8,5.8z M29.7,2.3c0,0.6-0.5,1.1-1.1,1.1c-0.6,0-1.1-0.5-1.1-1.1\
                          c0-0.7,0.5-1.1,1.1-1.1C29.2,1.2,29.7,1.6,29.7,2.3z"/>\
                        <path class="color" d="M37,10.8c0,5.9-1.5,7.2-3.2,7.2c-1.7,0-3.2-1.3-3.2-7.2c0-4.7,1.5-6.1,3.2-6.1C35.6,4.7,37,6,37,10.8z\
                           M34.9,10.8c0-4.2-0.4-5.1-1.1-5.1c-0.7,0-1.1,0.9-1.1,5.1c0,5.4,0.4,6.2,1.1,6.2C34.6,17,34.9,16.2,34.9,10.8z"/>\
                        <path class="color" d="M39.7,5.5L39.7,5.5c0.5-0.5,1-0.8,1.9-0.8c1.3,0,2.2,0.8,2.2,2.4l0,9.6c0,0.1,0,0.2,0.1,0.2l0.8,0.8\
                          c0.1,0,0.1,0.1,0.1,0.2c0,0.1-0.1,0.1-0.2,0.1h-3.5C41,18,41,18,41,17.9c0,0,0.1-0.1,0.1-0.2l0.8-0.8c0.1,0,0.1-0.1,0.1-0.2l0-9.6\
                          c0-0.9-0.3-1.4-0.9-1.4c-0.6,0-1.3,0.8-1.3,2.1l0,8.9c0,0.1,0,0.2,0,0.2l0.8,0.8c0.1,0,0.1,0.1,0.1,0.2c0,0.1-0.1,0.1-0.2,0.1H37\
                          c-0.1,0-0.2,0-0.2-0.1c0,0,0-0.1,0.1-0.2l0.8-0.8c0.1,0,0.1-0.1,0.1-0.2l0-10.9c0-0.1,0-0.1-0.1-0.2L37,4.9\
                          c-0.1-0.1-0.1-0.1-0.1-0.2c0-0.1,0.1-0.1,0.2-0.1l2.6,0c0.1,0,0.1,0,0.1,0.1L39.7,5.5z"/>\
                        <path class="color" d="M49.7,9.4c0,0.1-0.1,0.2-0.2,0.2c-0.1,0-0.1-0.1-0.2-0.2c-0.2-0.7-0.3-1.5-0.8-2.5\
                          c-0.3-0.8-0.7-1.2-1.2-1.2c-0.6,0-0.7,0.6-0.7,1.3c0,1,0.6,2.4,1.6,3.9c1,1.5,1.7,3.1,1.7,4.7c0,1.5-1.2,2.4-2.3,2.4\
                          c-0.8,0-1.2-0.3-1.6-0.8l-0.7,0.6c-0.1,0-0.2,0.1-0.3,0.1c-0.1,0-0.1-0.1-0.1-0.2l0-4.6c0-0.1,0-0.2,0.1-0.2c0.1,0,0.1,0.1,0.2,0.2\
                          c0.4,1.9,1,3.7,2,3.7c0.6,0,0.9-0.4,0.9-1c0-1.5-0.9-3.1-1.7-4.3c-1-1.4-1.5-2.9-1.5-4.4c0-1.6,1.1-2.6,2-2.6\
                          c0.6,0,1.1,0.3,1.6,0.8l0.8-0.6c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1,0.1,0.1,0.2L49.7,9.4z"/>\
                        <path class="color" d="M59.5,9.1c0.1,0,0.1,0,0.1,0.1c0,0,0,0.2-0.1,0.2l-0.8,0.7c0,3.6-0.3,5.2-1.2,6.2c0.1,0.2,0.3,0.4,0.5,0.4\
                          c0.4,0,0.9-0.5,1.1-0.8c0.1-0.1,0.2-0.1,0.2,0c0,0.7-0.5,1.9-1.7,1.9c-0.5,0-0.9-0.2-1.3-0.5C56,17.7,55.4,18,54.6,18\
                          c-2.4,0-3.8-1.7-3.8-4.2c0-2.3,0.9-3.6,1.9-4.9l0.5-0.6l-0.2-1c-0.3-1-0.6-2.4-0.6-4c0-1.9,1-3.3,2.5-3.3c1.3,0,2.1,1,2.1,2.7\
                          c0,2.3-1.1,4.1-1.7,5l-0.3,0.5L55,9c0.5,1.9,1.2,4.5,1.8,6c0.5-0.7,0.9-4.4,0.1-5l-0.7-0.5C56.1,9.4,56,9.3,56,9.3\
                          c0-0.1,0.1-0.1,0.2-0.1H59.5z M53.6,10c-0.7,0.8-1.1,1.9-1.1,3.5c0,2.1,1,3.4,2.1,3.4c0.4,0,0.8-0.1,1-0.4\
                          c-0.7-1.4-1.4-3.7-1.8-5.3L53.6,10z M54.7,6.3c0.6-0.8,1-2.1,1-3.5c0-0.9-0.1-1.7-0.9-1.7c-1.1,0-1.3,1.9-0.8,3.9l0.4,1.5L54.7,6.3\
                          z"/>\
                        <path class="color" d="M60.5,1.7c0-0.3-0.1-0.5-0.2-0.7l-0.8-0.7c-0.1-0.1-0.2-0.1-0.2-0.2c0-0.1,0.1-0.1,0.2-0.1l4.2,0\
                          c0.1,0,0.2,0.1,0.2,0.1c0,0.1-0.1,0.1-0.2,0.2l-0.8,0.7c-0.2,0.2-0.2,0.4-0.2,0.7l0,14.5c0,0.3,0.1,0.5,0.2,0.7l0.8,0.7\
                          c0.1,0.1,0.2,0.1,0.2,0.2c0,0.1-0.1,0.1-0.2,0.1h-4.2c-0.1,0-0.2-0.1-0.2-0.1c0-0.1,0.1-0.1,0.2-0.2l0.8-0.7\
                          c0.2-0.2,0.2-0.4,0.2-0.7L60.5,1.7z"/>\
                        <path class="color" d="M66.9,5.5L66.9,5.5c0.5-0.5,1-0.8,1.9-0.8c1.3,0,2.2,0.8,2.2,2.4l0,9.6c0,0.1,0,0.2,0.1,0.2l0.8,0.8\
                          c0.1,0,0.1,0.1,0.1,0.2c0,0.1-0.1,0.1-0.2,0.1h-3.5c-0.1,0-0.2,0-0.2-0.1c0,0,0.1-0.1,0.1-0.2l0.8-0.8c0.1,0,0.1-0.1,0.1-0.2l0-9.6\
                          c0-0.9-0.3-1.4-0.9-1.4c-0.6,0-1.3,0.8-1.3,2.1l0,8.9c0,0.1,0,0.2,0,0.2l0.8,0.8c0.1,0,0.1,0.1,0.1,0.2c0,0.1-0.1,0.1-0.2,0.1h-3.5\
                          c-0.1,0-0.2,0-0.2-0.1c0,0,0-0.1,0.1-0.2l0.8-0.8c0.1,0,0.1-0.1,0.1-0.2l0-10.9c0-0.1,0-0.1-0.1-0.2l-0.8-0.7\
                          C64.1,4.9,64,4.8,64,4.8c0-0.1,0.1-0.1,0.2-0.1l2.6,0c0.1,0,0.1,0,0.1,0.1V5.5z"/>\
                        <path class="color" d="M75.3,17.6c0,0.2-0.1,0.4-0.3,0.4h-0.8c-0.2,0-0.3-0.2-0.3-0.4L72,6c0-0.2-0.1-0.3-0.2-0.4l-0.6-0.6\
                          c-0.1-0.1-0.1-0.1-0.1-0.2c0-0.1,0.1-0.1,0.2-0.1l3.1,0c0.1,0,0.2,0,0.2,0.1c0,0.1,0,0.1-0.1,0.2l-0.7,0.7\
                          c-0.1,0.1-0.1,0.2-0.1,0.4l1.1,6.7h0.1l1-6.7c0-0.1,0-0.3-0.1-0.4l-0.7-0.7c-0.1-0.1-0.1-0.1-0.1-0.2c0,0,0.1-0.1,0.2-0.1h2.8\
                          c0.1,0,0.2,0,0.2,0.1c0,0-0.1,0.1-0.1,0.2l-0.6,0.6C77,5.6,77,5.8,77,6L75.3,17.6z"/>\
                        <path class="color" d="M79.4,12.8c0,3,0.5,4.2,1.7,4.2c0.9,0,1.3-0.4,1.9-1.1c0.1-0.1,0.1-0.1,0.2-0.1c0.1,0,0.1,0,0.1,0.1\
                          C82.9,17,82.1,18,80.7,18c-2.4,0-3.5-2.2-3.5-7.5c0-3.6,1.2-5.8,3.1-5.8c1.6,0,2.9,1,2.9,4.5l0,1.5c0,0.2-0.1,0.3-0.2,0.3h-3.6\
                          L79.4,12.8z M81.4,10l0-2.4c0-1.3-0.4-1.9-1-1.9c-0.7,0-1,0.7-1,1.9l0,2.4H81.4z"/>\
                        <path class="color" d="M88.5,9.4c0,0.1-0.1,0.2-0.2,0.2c-0.1,0-0.1-0.1-0.2-0.2c-0.2-0.7-0.3-1.5-0.8-2.5\
                          c-0.3-0.8-0.7-1.2-1.2-1.2c-0.6,0-0.7,0.6-0.7,1.3c0,1,0.6,2.4,1.6,3.9c1,1.5,1.7,3.1,1.7,4.7c0,1.5-1.2,2.4-2.3,2.4\
                          c-0.8,0-1.2-0.3-1.6-0.8l-0.7,0.6c-0.1,0-0.2,0.1-0.3,0.1c-0.1,0-0.1-0.1-0.1-0.2l0-4.6c0-0.1,0-0.2,0.1-0.2c0.1,0,0.1,0.1,0.2,0.2\
                          c0.4,1.9,1,3.7,2,3.7c0.6,0,0.9-0.4,0.9-1c0-1.5-0.9-3.1-1.7-4.3c-1-1.4-1.5-2.9-1.5-4.4c0-1.6,1.1-2.6,2-2.6\
                          c0.6,0,1.1,0.3,1.6,0.8l0.8-0.6c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1,0.1,0.1,0.2L88.5,9.4z"/>\
                        <path class="color" d="M91.9,16.1c0,0.5,0.3,0.9,0.6,0.9c0.3,0,0.7-0.3,0.8-0.5c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1,0.1,0.1,0.1\
                          c-0.2,0.7-0.7,1.5-1.9,1.5c-1.1,0-1.7-0.5-1.7-2.4l0-9.7c0-0.2-0.1-0.3-0.2-0.4l-0.6-0.6c-0.2-0.2-0.1-0.3,0-0.3c0,0,0.6,0,0.7,0\
                          c0.1,0,0.2-0.1,0.2-0.2c0,0,0-1,0-2.7c0-0.1,0.1-0.2,0.2-0.2l1.5,0c0.1,0,0.2,0,0.2,0.2l0,2.8c0,0.1,0,0.2,0.2,0.2h0.6\
                          c0.1,0,0.2,0.2,0,0.3l-0.6,0.6c-0.1,0.1-0.2,0.3-0.2,0.4L91.9,16.1z"/>\
                        <path class="color" d="M121,16.1c0,0.5,0.3,0.9,0.6,0.9c0.3,0,0.7-0.3,0.8-0.5c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1,0.1,0.1,0.1\
                          c-0.2,0.7-0.7,1.5-1.9,1.5c-1.1,0-1.7-0.5-1.7-2.4l0-9.7c0-0.2-0.1-0.3-0.2-0.4l-0.6-0.6c-0.2-0.2-0.1-0.3,0-0.3c0,0,0.6,0,0.7,0\
                          c0.1,0,0.2-0.1,0.2-0.2c0,0,0-1,0-2.7c0-0.1,0.1-0.2,0.2-0.2h1.5c0.1,0,0.2,0,0.2,0.2l0,2.8c0,0.1,0,0.2,0.2,0.2h0.6\
                          c0.1,0,0.2,0.2,0,0.3l-0.6,0.6c-0.1,0.1-0.2,0.3-0.2,0.4L121,16.1z"/>\
                        <path class="color" d="M94.5,5.8c0-0.1,0-0.1-0.1-0.2l-0.8-0.7c-0.1-0.1-0.1-0.1-0.1-0.2c0-0.1,0.1-0.1,0.2-0.1h2.6\
                          c0.1,0,0.1,0,0.1,0.1v0.7h0.1c0.5-0.5,0.9-0.8,1.9-0.8c0.8,0,1.4,0.4,1.7,0.8c0.5-0.5,1.1-0.8,2-0.8c1.2,0,2.1,0.8,2.1,2.4l0,9.6\
                          c0,0.1,0,0.2,0.1,0.2l0.8,0.8c0.1,0,0.1,0.1,0.1,0.2c0,0.1-0.1,0.1-0.2,0.1h-3.5c-0.1,0-0.2,0-0.2-0.1c0,0,0.1-0.1,0.1-0.2l0.8-0.8\
                          c0.1,0,0.1-0.1,0.1-0.2l0-9.6c0-0.9-0.2-1.4-0.8-1.4c-0.6,0-1.3,0.8-1.3,2.1l0,8.9c0,0.1,0,0.2,0.1,0.2l0.6,0.8\
                          c0.1,0,0.1,0.1,0.1,0.2c0,0.1-0.1,0.1-0.2,0.1h-3.1c-0.1,0-0.2,0-0.2-0.1c0,0,0.1-0.1,0.1-0.2l0.6-0.8c0.1,0,0.1-0.1,0.1-0.2l0-9.6\
                          c0-0.9-0.2-1.4-0.8-1.4c-0.6,0-1.3,0.8-1.3,2.1l0,8.9c0,0.1,0,0.2,0.1,0.2l0.8,0.8c0.1,0,0.1,0.1,0.1,0.2c0,0.1-0.1,0.1-0.2,0.1\
                          h-3.5c-0.1,0-0.2,0-0.2-0.1c0,0,0.1-0.1,0.1-0.2l0.8-0.8c0.1,0,0.1-0.1,0.1-0.2L94.5,5.8z"/>\
                        <path class="color" d="M107,12.8c0,3,0.5,4.2,1.7,4.2c0.9,0,1.3-0.4,1.9-1.1c0.1-0.1,0.1-0.1,0.2-0.1c0.1,0,0.1,0,0.1,0.1\
                          c-0.3,1.2-1.2,2.1-2.5,2.1c-2.4,0-3.5-2.2-3.5-7.5c0-3.6,1.2-5.8,3.1-5.8c1.6,0,2.9,1,2.9,4.5l0,1.5c0,0.2-0.1,0.3-0.2,0.3H107\
                          L107,12.8z M109,10l0-2.4c0-1.3-0.4-1.9-1-1.9c-0.7,0-1,0.7-1,1.9l0,2.4H109z"/>\
                        <path class="color" d="M113.5,5.5L113.5,5.5c0.5-0.5,1-0.8,1.9-0.8c1.3,0,2.2,0.8,2.2,2.4l0,9.6c0,0.1,0,0.2,0.1,0.2l0.8,0.8\
                          c0.1,0,0.1,0.1,0.1,0.2c0,0.1-0.1,0.1-0.2,0.1H115c-0.1,0-0.2,0-0.2-0.1c0,0,0.1-0.1,0.1-0.2l0.8-0.8c0.1,0,0.1-0.1,0.1-0.2l0-9.6\
                          c0-0.9-0.3-1.4-0.9-1.4c-0.6,0-1.3,0.8-1.3,2.1l0,8.9c0,0.1,0,0.2,0,0.2l0.8,0.8c0.1,0,0.1,0.1,0.1,0.2c0,0.1-0.1,0.1-0.2,0.1h-3.5\
                          c-0.1,0-0.2,0-0.2-0.1c0,0,0-0.1,0.1-0.2l0.8-0.8c0.1,0,0.1-0.1,0.1-0.2l0-10.9c0-0.1,0-0.1-0.1-0.2l-0.8-0.7\
                          c-0.1-0.1-0.1-0.1-0.1-0.2c0-0.1,0.1-0.1,0.2-0.1h2.6c0.1,0,0.1,0,0.1,0.1L113.5,5.5z"/>\
                        <path class="color" d="M127.7,9.4c0,0.1-0.1,0.2-0.2,0.2c-0.1,0-0.1-0.1-0.2-0.2c-0.2-0.7-0.3-1.5-0.8-2.5\
                          c-0.3-0.8-0.7-1.2-1.2-1.2c-0.6,0-0.7,0.6-0.7,1.3c0,1,0.6,2.4,1.6,3.9c1,1.5,1.7,3.1,1.7,4.7c0,1.5-1.2,2.4-2.3,2.4\
                          c-0.8,0-1.2-0.3-1.6-0.8l-0.7,0.6c-0.1,0-0.2,0.1-0.3,0.1c-0.1,0-0.1-0.1-0.1-0.2l0-4.6c0-0.1,0-0.2,0.1-0.2c0.1,0,0.1,0.1,0.2,0.2\
                          c0.4,1.9,1,3.7,2,3.7c0.6,0,0.9-0.4,0.9-1c0-1.5-0.9-3.1-1.7-4.3c-1-1.4-1.5-2.9-1.5-4.4c0-1.6,1.1-2.6,2-2.6\
                          c0.6,0,1.1,0.3,1.6,0.8l0.8-0.6c0.1,0,0.1-0.1,0.2-0.1c0.1,0,0.1,0.1,0.1,0.2L127.7,9.4z"/>\
                      </g>\
                    </svg>\
                    <span class="icon-arrow-right"></span>\</a>\
                  <!--<![endif]-->';
    let ieMenu = '<!--[if lt IE 9]><a href="//pionline.com"><img src="/img/logo.png"></span></a><![endif]-->';
    let logo   = '<!--[if !IE]><!--><a href="//pionline.com/conferences"><img src="/img/logo-conferences.svg" alt="P&I Conferences logo" title="P&I Conferences"></a><!--<![endif]-->';
    let ieLogo = '<!--[if lt IE 9]><a href="//pionline.com/conferences"><img src="/img/logo-conferences.jpg" alt="P&I Conferences logo" title="P&I Conferences"></a><![endif]-->';

    let headerLogo = this.props.conference.current.mobile_header_img ?
      '/' + this.props.conference.current.mobile_header_img :
      null;

    return <div className="header-wrap">
	      <section id="main-header" className="desktop clearfix">
          <div className="main-logo">
            <div className="svg-logo" dangerouslySetInnerHTML={{__html: menu }}></div>
            <div className="svg-logo" dangerouslySetInnerHTML={{__html: ieMenu }}></div>
            <nav id="parent-nav">
        			<ul>
        				<li><a href="//pionline.com/topics/latest-news">News</a></li>
        				<li><a href="//researchcenter.pionline.com/archive/piq">Searches &amp; Hires</a></li>
        				<li><a href="//researchcenter.pionline.com">Research Center</a></li>
        				<li><a href="//pionline.com/specialreports">Special Reports</a></li>
        				<li><a href="//video.pionline.com">Video</a></li>
        				<li><a href="//pionline.com/careers">Careers</a></li>
                <li><a href="//pionline.com/conferences">Conferences &amp; Webinars</a></li>
                <li><a href="//pionline.com/white-papers">White Papers</a></li>
        			</ul>
        		</nav>
		      </div>
      		{ /*<nav id="user-nav">
      			<ul className="clearfix">
      				<li><a href="//home.pionline.com/clickshare/addAccountFreeTrial.do?CSProduct=pionline&CSTargetURL=http://www.pionline.com/">Subscribe</a></li>
      				<li><a href="//home.pionline.com/clickshare/myhome.do"><span className="icon-person"></span></a></li>
      				<li><a href="//search.pionline.com"><span className="icon-search"></span></a></li>
      			</ul>
      		</nav>*/}
	      </section>
        <nav id="main-nav" className="clearfix">
      		<ul className="not-active">
      			<li><Link to={this.props.path}>Back to P&amp;I</Link></li>
            <li><IndexLink to={this.props.path + '/'} activeClassName="current">Home</IndexLink></li>
      			<li><Link to={this.props.path + '/agenda'} activeClassName="current">Agenda</Link></li>
            { !this.props.conference.current.hasSpeakers ? null :
      			  <li><Link to={this.props.path + '/speakers'} activeClassName="current">Speakers</Link></li>
      			}
            { !this.props.conference.current.hasAdvisoryBoard ? null :
              <li><Link to={this.props.path + '/advisors'} activeClassName="current">Advisory Board</Link></li>
            }
            { !this.props.conference.current.hasResources ? null :
        			<li><Link to={this.props.path + '/resources'} activeClassName="current">Resources</Link></li>
            }
            { !this.props.conference.current.hasExhibitors ? null :
        			<li><Link to={this.props.path + '/exhibitors'} activeClassName="current">Exhibitors</Link></li>
            }
            { !this.props.conference.current.hasSponsors ? null :
      			  <li><Link to={this.props.path + '/sponsors'} activeClassName="current">Sponsors</Link></li>
            }
      			<li><Link to={this.props.path + '/venue'} activeClassName="current">Venue</Link></li>
            { !this.props.conference.current.hasMultimedia ? null :
        			<li><Link to={this.props.path + '/multimedia'} activeClassName="current">Photos &amp; Video</Link></li>
            }
      			<li><Link to={this.props.path + '/contact'} activeClassName="current">Contact</Link></li>

            { this.props.conference.current.conference_id !== 'global-future-of-retirement' ? null :
              _.map(_.range(parseInt(this.props.conference.current.year)-1, 2013, -1), (v) => {
                // archived conferences
                return <li key={v}><a href={'//conferences.pionline.com' + this.props.path.split('/').slice(0, -1).join('/') + '/' + v}>{v}</a></li>
              })
            }
      		</ul>
      		{this.renderRegister(this.props.conference.current.registration)}
      	</nav>

      	<div id="secondary-header" className="clearfix">
          <img className="background-image" src={headerLogo} />
      		<div className="secondary-logo">
            <div className="svg-logo" dangerouslySetInnerHTML={{__html: logo }}></div>
            <div className="svg-logo" dangerouslySetInnerHTML={{__html: ieLogo }}></div>
      		</div>
      		<div className="main-hamburger">
      			<span className="one"></span>
      			<span className="two"></span>
      			<span className="three"></span>
      		</div>
          <div className="conference-info">
      			<h1><Link to={this.props.path}>{this.props.conference.current.name}</Link></h1>
            { _.size(this.props.conference.current.cities) > 1 ?
                 _.map(this.props.conference.current.cities, (city, k) => {
                  return <time classNameName="date" key={city.city}>
                    <Link to={this.props.path + '/agenda/' + city.city}>
                      {[moment.unix(city.start).utcOffset('+0500').format('MMM. D,'), city.city].join(' ')}
                    </Link>
                    <span className="spacer">{ k < this.props.conference.current.cities.length - 1 ? '•' : null }</span>
                  </time>
                }) :
                _.map(this.props.conference.current.cities, (city) => {
                  return <time classNameName="date" key={city._id}>
                    {[
                      [
                        moment.unix(city.start).utcOffset('+0500').format('MMMM D'),
                        city.end ? moment.unix(city.end).utcOffset('+0500').format('D') : ''].join(city.end ? '-' : ''),
                      city.city
                    ].join(' • ')}
                  </time>
                })
              }
      			{this.renderRegister(this.props.conference.current.registration)}
      		</div>
      	</div>
      </div>
  }
}

export default Header;
