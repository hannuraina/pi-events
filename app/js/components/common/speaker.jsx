'use strict';

import _       from 'lodash';
import React   from 'react';
import {Link}  from 'react-router';
import flag    from 'countryjs';

class Speaker extends React.Component {
  propTypes: {
    speaker:   React.PropTypes.object.required,
    showPhoto: React.PropTypes.boolean.optional,
    showBio:   React.PropTypes.boolean.optional,
    showFlag: React.PropTypes.boolean.optional
  }

  constructor(props) {
    super(props);
  }

  renderFlags = (country) => {
    if (!country) country = 'United States';

    country = country.split('/').map((c) => {
      let iso = flag.ISOcodes(c, 'name');
      let isoFlag = iso ? iso.alpha2.toLowerCase() : 'us';
      return <h1><span key={isoFlag} className={'flag-icon flag-icon-' + isoFlag}></span></h1>;
    });

    return country;
  }

  render() {
    let speakerUrl   = [this.props.path, 'speakers', this.props.speaker._id].join('/');
    let photo        = '/' + (this.props.speaker.photo ? this.props.speaker.photo : 'img/person.gif');

    return <div className="speaker">
      <div className="info">
        {!this.props.showPhoto ? null : <Link to={speakerUrl}><img src={photo}></img></Link>}
        <div className="flags">{!this.props.showFlag ? null : this.renderFlags(this.props.speaker.country)}</div>
        <div className="name">
          <Link to={speakerUrl}>{this.props.speaker.name}</Link>
        </div>
        <div className="accreditation" dangerouslySetInnerHTML={{__html: this.props.speaker.accreditation }} />
        <div className="company" dangerouslySetInnerHTML={{__html: this.props.speaker.company }} />
        <div className="position" dangerouslySetInnerHTML={{__html: this.props.speaker.title }} />
      </div>
      { !this.props.showBio ? null :
        <div className="description">
          <div dangerouslySetInnerHTML={{__html: this.props.speaker.bio}}/>
        </div>
      }
    </div>
  }
}

export default Speaker;
