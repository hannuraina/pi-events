'use strict';

import _ from 'lodash';
import React from 'react';

import Listing from '../common/listing';
import Speaker from '../common/speaker';

class Advisors extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {

    let advisors =  _(this.props.conference.current.speakers)
      .filter((advisor) => { return !!advisor.advisory_board; })
      .sortBy((advisor) => { return _.last(advisor.name.split(' ')); })
      .map((advisor) => { return <Speaker key={advisor._id} speaker={advisor} showPhoto={true} {...this.props} /> })
      .value();

    let advisor = this.props.conference.current.speakers ?
      this.props.conference.current.speakers[this.props.params.advisor_id] :
      null;

    return <div className="speakers">
      <section className="section">
        <div className="wrap-one clearfix">
          <div className="title">Advisory Board</div>
          { !advisor ? null : <Speaker key={advisor._id} speaker={advisor} showPhoto={true} showBio={true} {...this.props} /> }
        </div>
      </section>
      <section className="section">
        <Listing elements={advisors} />
      </section>
    </div>
  }
}

export default Advisors;
