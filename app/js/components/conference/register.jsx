'use strict';

import _ from 'lodash';
import React from 'react';

class Register extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let html = this.props.conference.current.registration ?
      this.props.conference.current.registration.html : null;

    return <div className="venues">
      <section className="section">
        <div className="html wrap-two clearfix">
          <div className="title">Register</div>
          <div className="summary" dangerouslySetInnerHTML={{__html: html}} />
        </div>
      </section>
    </div>
  }
}

export default Register;
