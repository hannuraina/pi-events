'use strict';

import _ from 'lodash';
import React from 'react';
import {Link} from 'react-router';
import Listing from '../common/listing';
import Sponsor from '../common/sponsor';

class Exhibitors extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    let exhibitors =  _(this.props.conference.current.sponsors)
      .filter((exhibitor) => { return exhibitor.level === 'exhibitor' })
      .map((exhibitor) => { return <Sponsor key={exhibitor._id} sponsor={exhibitor} showPhoto={true} {...this.props} /> })
      .value();

    let exhibitor = this.props.conference.current.sponsors ?
      this.props.conference.current.sponsors[this.props.params.sponsor_id] :
      null;

    return <div className="sponsors">
      <section className="section">
        <div className="opportunities wrap-two clearfix">
          <div className="title">Exhibitors</div>
          { !exhibitor ? null : <Sponsor key={exhibitor._id} sponsor={exhibitor} showPhoto={true} showBio={true} {...this.props} /> }
          { !this.props.conference.current.become_sponsor_email || exhibitor ? null :
            <Link
              to={'//mailto:' + this.props.conference.current.become_sponsor_email}
              className="button button-green">Partner with us</Link>
          }
        </div>
      </section>
      <section className="section">
        <Listing elements={exhibitors} title="" />
      </section>
    </div>
  }
}

export default Exhibitors;
