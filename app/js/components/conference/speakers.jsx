'use strict';

import _ from 'lodash';
import React from 'react';

import Listing from '../common/listing';
import Speaker from '../common/speaker';

class Speakers extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let showSpeakerFlags = this.props.conference.current.conference_id === 'global-future-of-retirement';

    let featured = _(this.props.conference.current.speakers)
      .filter({featured_speaker: 'yes'})
      .sortBy((speaker) => { return _.last(speaker.name.trim().split(' ')); })
      .map((speaker) => { return <Speaker key={speaker._id} speaker={speaker} showPhoto={true} showFlag={showSpeakerFlags} {...this.props} />})
      .value();

    // if no speakers are marked as featured default to chairpeople or keynotes
    featured = featured.length ? featured : _(this.props.conference.current.speakers)
      .filter((speaker) => { return !!speaker.keynote || !!speaker.chairperson; })
      .sortBy((speaker) => { return _.last(speaker.name.trim().split(' ')); })
      .map((speaker) => { return <Speaker key={speaker._id} speaker={speaker} showPhoto={true} showFlag={showSpeakerFlags} {...this.props} /> })
      .value();

    let speakers =  _(this.props.conference.current.speakers)
      .reject((speaker) => { return speaker.advisory_board_only; })
      .sortBy((speaker) => { return _.last(speaker.name.trim().split(' ')); })
      .map((speaker) => { return <Speaker key={speaker._id} speaker={speaker} showPhoto={true} showFlag={showSpeakerFlags} {...this.props} /> })
      .value();

    let speaker = this.props.conference.current.speakers ?
      this.props.conference.current.speakers[this.props.params.speaker_id] :
      null;

    return <div className="speakers">
      <section className="section">
        <div className="opportunities wrap-three clearfix">
          <div className="title">Speakers</div>
          { !speaker ? null : <Speaker key={speaker._id} speaker={speaker} showPhoto={true} showBio={true} showFlag={showSpeakerFlags} {...this.props} /> }
          { !this.props.conference.current.become_speaker_email || speaker ? null :
            <a
              className="button button-green"
              href={'mailto:' + this.props.conference.current.become_speaker_email}>Speaking Opportunities
            </a>
          }
        </div>
      </section>
      { speaker ? null :
        <section className="section">
          <Listing elements={featured} title="Featured Speakers" />
        </section>
      }
      <section className="section">
        <Listing elements={speakers} title="All Speakers" />
      </section>
    </div>
  }
}

export default Speakers;
