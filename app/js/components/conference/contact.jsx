'use strict';

import _ from 'lodash';
import React from 'react';

class Contact extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let contactHtml = this.props.conference.current.data ?
      this.props.conference.current.data.contact.contact :
      null;

    return <div className="contact">
      <section className="section">
        <div className="html wrap-two clearfix">
          <div className="title">Contact</div>
          <div className="summary" dangerouslySetInnerHTML={{__html: contactHtml}} />
        </div>
      </section>
    </div>
  }
}

export default Contact;
