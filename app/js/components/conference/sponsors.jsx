'use strict';

import _ from 'lodash';
import React from 'react';
import Listing from '../common/listing';
import Sponsor from '../common/sponsor';

class Sponsors extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    let levels =  _(this.props.conference.current.sponsors)
      .reject((sponsor) => { return sponsor.level === 'exhibitor' })
      .sortBy((sponsor) => { return ['diamond', 'platinum', 'gold'].indexOf(sponsor.level); })
      .map((sponsor) => { return <Sponsor key={sponsor._id} sponsor={sponsor} showLevel={true} showPhoto={true} {...this.props} /> })
      .groupBy('props.sponsor.level')
      .value();

    let sponsor = this.props.conference.current.sponsors ?
      this.props.conference.current.sponsors[this.props.params.sponsor_id] :
      null;

    return <div className="sponsors">
      <section className="section">
        <div className="opportunities wrap-two clearfix">
          <div className="title">Sponsors</div>
          { !sponsor ? null : <Sponsor key={sponsor._id} sponsor={sponsor} showPhoto={true} showBio={true} {...this.props} /> }
          { !this.props.conference.current.become_sponsor_email || sponsor ? null :
            <a
              className="button button-green"
              href={'mailto:' + this.props.conference.current.become_sponsor_email}>Partner with us
            </a>
          }
        </div>
      </section>
      { _.map(levels, (level, k) => {
        return <section key={k} className="section">
          <Listing elements={level} title={!k || k === 'sponsor' ? 'Lead Sponsors' : k} />
        </section>
      })}
    </div>
  }
}

export default Sponsors;
