'use strict';

import _ from 'lodash';
import React from 'react';

class Venues extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      city: props.params.venue_id ?
        props.params.venue_id :
        props.conference.current.cities ? _(props.conference.current.cities).first().city : null
    };
  }

  componentWillReceiveProps = (nextProps) => {
    this.setState({
      city:  nextProps.params.venue_id ?
        nextProps.params.venue_id :
        _(nextProps.conference.current.cities).first().city
    });
  }

  handleCityChange = (evt) => {
    this.setState({ city: evt.target.name });
  }

  render() {
    return <div className="venues">
      <section className="section">
        <div className="html wrap-three clearfix">
          <div className="title">Venue</div>
            <div className="tab-nav wrap-one clearfix">
              <ul>
                { _.map(this.props.conference.current.cities, (city) => {
                  return <li key={city._id} className={this.state.city === city.city ? 'current' : null}>
                    <a onClick={this.handleCityChange} name={city.city}>{city.city}</a>
                  </li>
                })}
              </ul>
            </div>
        </div>
      </section>

      <section className="section wrap-three no-border">
        { _.map(this.props.conference.current.cities, (city, k) => {
          if (city.city === this.state.city) {
            return <div
              key={k}
              className="summary"
              dangerouslySetInnerHTML={{__html: city.venue_html ?
                city.venue_html :
                this.props.conference.current.venue.html }}>
            </div>
          }
        })}
      </section>
    </div>
  }
}

export default Venues;
