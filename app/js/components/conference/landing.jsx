'use strict';

import _ from 'lodash';
import React from 'react';

import Masthead from './masthead';
import Header from '../common/header';
import Listing  from '../common/listing';
import Speaker  from '../common/speaker';
import Sponsor  from '../common/sponsor';
import Event    from '../common/event';

class Landing extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let speakers = _(this.props.conference.current.speakers)
      .filter({featured_speaker: 'yes'})
      .sortBy((speaker) => { return _.last(speaker.name.trim().split(' ')); })
      .map((speaker) => { return <Speaker key={speaker._id} speaker={speaker} showPhoto {...this.props} />})
      .value();

    // if no speakers are marked as featured default to chairpeople or keynotes
    speakers = speakers.length ? speakers : _(this.props.conference.current.speakers)
      .filter((speaker) => { return !!speaker.keynote || !!speaker.chairperson; })
      .sortBy((speaker) => { return _.last(speaker.name.trim().split(' ')); })
      .map((speaker) => { return <Speaker key={speaker._id} speaker={speaker} showPhoto {...this.props} /> })
      .value();

    let sponsors = _(this.props.conference.current.sponsors)
      .sortBy((sponsor) => { return ['lead-sponsor', 'co-sponsor', 'diamond', 'platinum', 'gold'].indexOf(sponsor.level); })
      .reject((sponsor) => { return sponsor.level === 'exhibitor'; })
      .map((sponsor) => { return <Sponsor key={sponsor._id} sponsor={sponsor} {...this.props} /> })
      .value();

    let other = _(this.props.conferences.upcoming)
      .reject({ _id: this.props.conference.current._id })
      .slice(0, 4)
      .map((conference) => { return <Event key={conference._id} event={conference} image /> })
      .value();

    return <div>
      <Masthead conference={this.props.conference.current} path={this.props.path}/>
      { !speakers.length ? null :
        <section className="section featured-speakers">
          <Listing
            elements={speakers}
            limit={3}
            title="Featured Speakers"
            buttonText="View All Speakers"
            buttonLink={this.props.location.pathname + '/speakers'}/>
        </section>
      }
      { !sponsors.length ? null :
        <section className="section featured-sponsor">
          <Listing
            elements={sponsors}
            limit={6}
            title="Sponsors"
            buttonText="View All Sponsors"
            buttonLink={this.props.location.pathname + '/sponsors'}/>
        </section>
      }
      <section className="section other">
        <Listing
          elements={other}
          limit={4}
          title="Other P&I Conferences"
          buttonText="More info on P&I Conferences"
          buttonLink="//www.pionline.com/conferences?event_type=conference#events" />
      </section>
    </div>
  }
};

export default Landing;
