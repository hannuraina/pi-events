'use strict';

import _ from 'lodash';
import React from 'react';

import connectToStores   from 'alt/utils/connectToStores';
import ConferenceActions from '../../actions/conference.actions';
import ConferenceStore   from '../../stores/conference.store';
import ConferencesStore   from '../../stores/conferences.store';

import Header  from '../common/header';
import Footer  from '../common/footer';

@connectToStores
class Index extends React.Component {

  constructor(props) {
    super(props);
    this.state = { view: '' };
    this.onLoadData();
  }

  static getStores(props) {
    return [ConferenceStore, ConferencesStore];
  }

  static getPropsFromStores(props) {
    return {
      conference:  ConferenceStore.getState(),
      conferences: ConferencesStore.getState()
    };
  }

  onLoadData = () => {
    // load individual conference
    ConferenceActions.load({
      type:          'conference',
      conference_id: this.props.params.conference_id,
      year:          this.props.params.year,
      limit:         1
    });

    // load additional conferences
    ConferenceActions.loadAll({
      type:   'conference',
      fields: 'name,date,locations,conference_description,marketing_img'
    });
  }

  render() {
    let path = [
      '/conference',
      this.props.params.conference_id,
      this.props.params.year
    ].join('/');

    let view = _.last(this.props.location.pathname.split('/'));

    return <div>
      <Header path={path} conference={this.props.conference} marketing={this.props.conference.marketing}/>
      <main>
        {React.cloneElement(this.props.children, {path: path, ...this.props})}
      </main>
      <Footer conference={this.props.conference} path={path} view={view} {...this.props} />
    </div>
  }
};

export default Index;
