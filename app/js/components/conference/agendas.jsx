'use strict';

import _ from 'lodash';
import moment from 'moment';
import React from 'react';
import {Link} from 'react-router';
import Agenda from '../common/agenda';

class Agendas extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      city: props.params.agenda_id ?
        props.params.agenda_id :
        props.conference.current.cities ? _(props.conference.current.cities).first().city : null
    };
  }

  componentWillReceiveProps = (nextProps) => {
    this.setState({
      city:  nextProps.params.agenda_id ?
        nextProps.params.agenda_id :
        _(nextProps.conference.current.cities).first().city
    });
  }

  handleCityChange = (evt) => {
    this.setState({ city: evt.target.name });
  }

  render() {
    let cities     = {};
    let location   = _(this.props.conference.current.cities).find({ city: this.state.city });
    let agendas    = _(this.props.conference.current.agendas).values().value();

    return <div className="agendas">
      <section className="section">
        <div className="html wrap-three clearfix">
          <div className="title">Agenda</div>
          { !this.props.conference.current.agenda_html ? null :
            <div className="summary" dangerouslySetInnerHTML={{__html: this.props.conference.current.agenda_html }}></div>
          }
        </div>
      </section>
      { !agendas ? null :
        <div>
          <div className="tab-nav wrap-one clearfix">
            <ul>
              { _.map(this.props.conference.current.cities, (city) => {
                return <li key={city._id} className={this.state.city === city.city ? 'current' : null}>
                  <a onClick={this.handleCityChange} name={city.city}>{city.city}</a>
                </li>
              })}
            </ul>
          </div>
          <section className="section wrap-one no-border">
            { _.map(agendas, (agenda, k) => {
              if (!cities[this.state.city] || !agenda.event_location || _.includes(agenda.event_location, this.state.city)) {
                cities[this.state.city] = true;
                return <div key={k}>
                  { !agendas[k-1] || agendas[k-1].event_date !== agenda.event_date ?
                    <div name={agenda.event_date} key={agenda.event_date} className="agenda-info wrap-one clearfix">
                      <ul>
                        <li><Link to={this.props.path + '/venue/' + this.state.city}>
                          <span className="icon-location"></span>{location.venue ? location.venue : 'Venue'}
                        </Link></li>
                        <li><a><span className="icon-calendar"></span>
                          {moment.unix(agenda.event_date ? agenda.event_date : location.start).utcOffset('+0500').format('dddd, MMM. D')}
                        </a></li>
                        <li>
                          { location.agenda_full ?
                            <a href={'/' + location.agenda_full}><span className="icon-download"></span>Agenda PDF</a> :
                            <Link to={this.props.path + '/agenda'}><span className="icon-download"></span>Agenda</Link>
                          }
                        </li>
                      </ul>
                    </div> : null
                  }
                  <Agenda key={agenda._id} agenda={agenda} {...this.props} />
                </div>
              }
            })}
          </section>
        </div>
      }
    </div>
  }
}

export default Agendas;
