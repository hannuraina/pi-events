'use strict';

import _ from 'lodash';
import React from 'react';
import {Link} from 'react-router';

import Event from '../common/event';

class Masthead extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return <section className="section no-border">
      <div className="masthead wrap-one clearfix">
        <Event
          event={this.props.conference}
          formatDate='dddd, MMM. D'
          locations={true}
          image={true}
          inverse={true} />
        <div className="links">
          <Link to={this.props.path + '/agenda'} className="button button-gray">Agenda<span className="icon-arrow-right"></span></Link>
          <Link to={this.props.path + '/speakers'} className="button button-gray">Speakers<span className="icon-arrow-right"></span></Link>
          <Link to={this.props.path + '/venue'} className="button button-gray">Venues<span className="icon-arrow-right"></span></Link>
        </div>
        <div className="summary wrap-two">{this.props.conference.summary}</div>
      </div>
    </section>
  }
}

export default Masthead;
