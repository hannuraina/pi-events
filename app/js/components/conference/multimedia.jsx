'use strict';

import _ from 'lodash';
import React from 'react';

class Multimedia extends React.Component {

  constructor(props) {
    super(props);
  }

  renderLightBox = () => {
    setTimeout(() => {
      let featured      = $('.media-wrap a');
      let all           = $('#all-photos');
      let featuredChild = $('#all-photos figure:first-child a');

      if (!$._data(featured[0]).events) {
        all.slickLightbox({itemSelector:'figure a'});
        featured.on('click', (evt) => { evt.preventDefault(), featuredChild.trigger('click') });
      }
    });
  }

  render() {
    let renderLightBox = this.renderLightBox();
    let featuredPhoto  = this.props.conference.current.media ?
      _(this.props.conference.current.media.slideshows[0].images).values().first() :
      null;

    return <div className="multimedia">
      <section className="section">
        <div className="wrap-one clearfix">
          <div className="title">Photo &amp; Video</div>
        </div>
      </section>
      { !featuredPhoto ? null :
        <div>
          <div id="featured-photo-video" className="secondary-section media">
          	<div className="wrap-one">
          		<div className="bio-wrap clearfix">
                <figure className="photo">
            			<div className="media-wrap">
              			<a href={[featuredPhoto, '?imageversion=widescreen&maxH=250'].join('')} target="_blank">
                      <span className="icon-slideshow"></span>
              			  <img src={[featuredPhoto, '?imageversion=widescreen&maxH=250'].join('')} />
                    </a>
              		</div>
            			<figcaption>
                    <h3>Slide Show</h3>
                    <div>Photos From the Event</div>
                  </figcaption>
            		</figure>
              </div>
            </div>
          </div>
          <section id="all-photos" className="section no-border video-photo media" >
            <div className="photos wrap-one clearfix">
              <div className="title sub">All Photos</div>
          		<div className="bio-wrap clearfix">
          			{ _.map(this.props.conference.current.media.slideshows[0].images, (photo, k) => {
                  return <figure key={k} className="photo">
  	        			  <a href={[photo, '?imageversion=widescreen&maxH=500'].join('')} target="_blank">
                      <img src={[photo, '?imageversion=widescreen&maxH=250'].join('')} />
                    </a>
  	        		  </figure>
                })}
              </div>
            </div>
          </section>
        </div>
      }
    </div>
  }
}

export default Multimedia;
