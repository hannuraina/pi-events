'use strict';

import _ from 'lodash';
import React from 'react';

import Listing from '../common/listing';
import Resource from '../common/resource';

class Resources extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let whitepapers =  _(this.props.conference.current.white_papers)
      .map((whitepaper) => {
        return <Resource key={whitepaper._id} showPhoto={true} resource={whitepaper} type="whitepaper" {...this.props} />
      })
      .value();

    let supplements =  _(this.props.conference.current.supplements)
        .map((supplement) => {
          return <Resource key={supplement._id} showPhoto={true} type="supplement" resource={supplement} {...this.props} />
        })
        .value();

    return <div className="resources">
      <section className="section">
        <div className="wrap-one clearfix">
          <div className="title">Resources</div>
        </div>
      </section>
      <section className="section featured-resource">
        <Listing elements={supplements.slice(0, 1)} />
      </section>
      { supplements.length < 2 ? null :
        <section className="section supplements">
          <Listing elements={supplements.slice(1)} />
        </section>
      }
      <section className="section whitepapers">
        <Listing elements={whitepapers} />
      </section>

    </div>
  }
}

export default Resources;
